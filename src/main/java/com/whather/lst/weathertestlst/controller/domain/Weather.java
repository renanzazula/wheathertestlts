package com.whather.lst.weathertestlst.controller.domain;

import java.io.Serializable;

public class Weather implements Serializable {

    private static final long serialVersionUID = -6612762288260227887L;

    private String condition;
    private String temperature;

    public Weather() {
    }

    public Weather(String condition, String temperature) {
        this.condition = condition;
        this.temperature = temperature;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "condition='" + condition + '\'' +
                ", temperature='" + temperature + '\'' +
                '}';
    }
}
