package com.whather.lst.weathertestlst.controller;

import com.whather.lst.weathertestlst.controller.domain.Weather;
import com.whather.lst.weathertestlst.controller.functions.DomainAdapter;
import com.whather.lst.weathertestlst.service.WeatherService;
import com.whather.lst.weathertestlst.service.WeatherServiceImpl;
import com.whather.lst.weathertestlst.utils.ConstantsApi;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Api(value = "Weather Controller", tags = "weather")
@RestController
@RequestMapping(ConstantsApi.WEATHER_URL)
public class WeatherController {

    private final WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping({"/{city}"})
    public ResponseEntity<Weather> getWeather(@PathVariable String city) throws IOException, InterruptedException {
        return new ResponseEntity<>(DomainAdapter.weatherDtoToWeatherAdapter.apply(weatherService.getWeather(city)), HttpStatus.OK);
    }
}
