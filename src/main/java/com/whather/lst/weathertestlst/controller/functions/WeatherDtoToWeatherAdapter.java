package com.whather.lst.weathertestlst.controller.functions;


import com.whather.lst.weathertestlst.controller.domain.Weather;
import com.whather.lst.weathertestlst.service.dto.WeatherDto;

import java.util.function.Function;

public class WeatherDtoToWeatherAdapter implements Function<WeatherDto, Weather> {

    @Override
    public Weather apply(WeatherDto weatherDto) {
        Weather weather = new Weather();
        weather.setCondition(weatherDto.getCondition());
        weather.setTemperature(weatherDto.getTemperature());
        return weather;
    }
}
