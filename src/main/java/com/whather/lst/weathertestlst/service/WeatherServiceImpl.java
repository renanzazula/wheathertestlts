package com.whather.lst.weathertestlst.service;

import com.whather.lst.weathertestlst.repository.WeatherRepository;
import com.whather.lst.weathertestlst.repository.dto.Root;
import com.whather.lst.weathertestlst.service.dto.WeatherDto;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class WeatherServiceImpl implements WeatherService{

    private final WeatherRepository weatherRepository;

    public WeatherServiceImpl(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    @Override
    public WeatherDto getWeather(String city) throws IOException, InterruptedException {
        Root root = weatherRepository.getWeather(city);
        WeatherDto weather = new WeatherDto();
        weather.setCondition(root.getWeather().get(0).getMain() + " " + root.getWeather().get(0).getDescription());
        weather.setTemperature(String.valueOf(root.getMain().getTemp()));
        return weather;
    }

}
