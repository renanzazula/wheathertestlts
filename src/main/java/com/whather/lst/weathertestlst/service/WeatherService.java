package com.whather.lst.weathertestlst.service;

import com.whather.lst.weathertestlst.service.dto.WeatherDto;

import java.io.IOException;

public interface WeatherService {

    WeatherDto getWeather(String city) throws IOException, InterruptedException;

}
