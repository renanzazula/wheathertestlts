package com.whather.lst.weathertestlst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherTestLstApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherTestLstApplication.class, args);
    }

}
