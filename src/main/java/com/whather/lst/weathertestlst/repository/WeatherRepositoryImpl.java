package com.whather.lst.weathertestlst.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.whather.lst.weathertestlst.repository.dto.Root;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class WeatherRepositoryImpl implements WeatherRepository {

    @Value("${weather.url}")
    private String weatherURL;

    @Value("${weather.api.key}")
    private String APIKey;

    @Override
    public Root getWeather(String city) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(weatherURL + "?q=" + city + "&appid=" + APIKey)).GET().build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        ObjectMapper om = new ObjectMapper();
        Root root = om.readValue(response.body(), Root.class);

        return root;
    }

}
