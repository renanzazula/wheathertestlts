package com.whather.lst.weathertestlst.repository;

import com.whather.lst.weathertestlst.repository.dto.Root;

import java.io.IOException;

public interface WeatherRepository {

     Root getWeather(String city) throws IOException, InterruptedException;

}
