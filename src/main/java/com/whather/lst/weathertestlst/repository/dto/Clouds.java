package com.whather.lst.weathertestlst.repository.dto;

import java.io.Serializable;

public class Clouds implements Serializable {
    public int all;

    public Clouds() {
    }

    public Clouds(int all) {
        this.all = all;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}