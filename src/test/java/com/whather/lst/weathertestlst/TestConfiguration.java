package com.whather.lst.weathertestlst;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages={"com.whather.lst.weathertestlst"})
@PropertySource("classpath:application.properties")
public class TestConfiguration{ }
