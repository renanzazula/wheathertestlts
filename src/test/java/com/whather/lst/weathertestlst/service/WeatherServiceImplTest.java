package com.whather.lst.weathertestlst.service;


import com.whather.lst.weathertestlst.TestConfiguration;
import com.whather.lst.weathertestlst.repository.WeatherRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { TestConfiguration.class }, loader = AnnotationConfigContextLoader.class)
public class WeatherServiceImplTest {

    @Autowired
    private WeatherRepository repository;

    private WeatherService service;

    private String city;

    @BeforeEach
    public void setUp() {
        service = new WeatherServiceImpl(repository);
        city = "Barcelona";
    }

    @Test
    public void getWeather() throws IOException, InterruptedException {
       assertNotNull(service.getWeather(city));
    }
}

