package com.whather.lst.weathertestlst.repository;

import com.whather.lst.weathertestlst.TestConfiguration;
import com.whather.lst.weathertestlst.service.WeatherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { TestConfiguration.class }, loader = AnnotationConfigContextLoader.class)
class WeatherRepositoryImplTest {

    @Value("${weather.url}")
    private String weatherURL;

    @Value("${weather.api.key}")
    private String APIKey;

    private String city;

    @BeforeEach
    public void setUp() {
        city = "Barcelona";
    }

    @Test
    public void getWeather() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(weatherURL + "?q=" + city + "&appid=" + APIKey)).GET().build();
        HttpResponse<String> found = client.send(request, HttpResponse.BodyHandlers.ofString());
        assertEquals(found.statusCode(), 200);
    }
}