package com.whather.lst.weathertestlst.controller;

import com.whather.lst.weathertestlst.controller.domain.Weather;
import com.whather.lst.weathertestlst.repository.WeatherRepository;
import com.whather.lst.weathertestlst.service.WeatherService;
import com.whather.lst.weathertestlst.service.WeatherServiceImpl;
import com.whather.lst.weathertestlst.service.dto.WeatherDto;
import com.whather.lst.weathertestlst.utils.ConstantsApi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {WeatherController.class})
public class WeatherControllerTest extends AbstractRestControllerTest {

    @MockBean
    WeatherService service;

    @Autowired
    MockMvc mockMvc;

    private String city;
    private WeatherDto weather;

    @BeforeEach
    public void setUp() {
        city = "Barcelona";
        weather = new WeatherDto("Clear clear sky", "289.57");
    }

    @Test
    public void getWeather() throws Exception {
        Mockito.when(service.getWeather(city)).thenReturn(weather);
        mockMvc.perform(get(ConstantsApi.WEATHER_URL + "/Barcelona")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}